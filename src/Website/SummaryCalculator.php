<?php

namespace Website;

use Pimcore\Model\Object\Concrete; 

class SummaryCalculator {
	
	public static function renderLayoutText($data, $object,$params){
		$blockItems = $object->getBlock_items();
		$finalHtml = "";
		foreach($blockItems as $block){
			//start composing the final html
			$finalHtml .= '<div class="itemGeneralContainer" style="border-style:solid;border-radius:25px; margin-bottom:15px; padding-left:15px;">'
			.'<p class="itemSpecific">'.$block["codice_ean"]->getData().'-'.$block["descrizione_articolo"]->getData().'<br>'
			.'<strong>Note</strong>: ' .$block["note_buyer"]->getData().'</p>'
			.'<hr>'
			.'<div class="tableContainer">'
			.'<table style="width:100%;">'
			.'<tr> <th>Prezzatura</th><th>Prz.listino</th><th>Prz. vend. consigliato</th><th>Budget proposto</th><th>Tigros</th><th>Iperal</th><th>Poli</th><th>Sogegross</th></tr>' 
			.'<tr><td>'.$block["prezzatura"]->getData().'</td><td>'.$block["prezzo_listino"]->getData().'</td><td>'.$block["prezzo_di_vendita_consigliato"]->getData().'</td><td>'.$block["budget_proposto"]->getData().'</td>';
			if($block["interesse_merceologico_tigros"] ->getData() == 1){
				$finalHtml .= '<td>V</td>';
			} else{
				$finalHtml .= '<td>X</td>';
			}
			if($block["interesse_merceologico_iperal"] ->getData() == 1){
				$finalHtml .= '<td>V</td>';
			} else{
				$finalHtml .= '<td>X</td>';
			}
			if($block["interesse_merceologico_poli"] ->getData() == 1){
				$finalHtml .= '<td>V</td>';
			} else{
				$finalHtml .= '<td>X</td>';
			}
			if($block["interesse_merceologico_sogegross"] ->getData() == 1){
				$finalHtml .= '<td>V</td>';
			} else{
				$finalHtml .= '<td>X</td>';
			}
			//attach the final html part of the table
			$finalHtml .= '</tr>'
			 .'</table>'
			.'</div></div>';
		}
			return $finalHtml;
		}
}

