<?php

namespace Website;

use Pimcore\Model\Object\Concrete; 

class ListingCalculator {
	public static $tigrosSuffix = "_tigros";
	public static $poliSuffix = "_poli";
	public static $iperalSuffix = "_iperal";
	public static $sogegrossSuffix = "_sogegross";
	//Da sostituire con una immagine in locale
	public static $urlOkIcon = "Ok";
	public static $urlKoIcon = "Ko";
	
	/**
     * @param $data string as provided in the class definition
     * @param $object Concrete
     * @param $params mixed
     * @return string
	 */
	public static function renderLayoutText($data, $object, $params) {
				$field_name = $data;
				$res = "";
				$blockItems = $object->getBlock_items();				
				foreach($blockItems as $value){
				if($field_name == ListingCalculator::$tigrosSuffix){
				if($value["interesse_merceologico_tigros"] ->getData() == 1){
					//$res = '<img src="'.ListingCalculator::$urlOkIcon.'> width="30" heigth="30" ">';
					return "ok";
				} else {
					return"ko";
					//$res = '<img src="'.ListingCalculator::$urlKoIcon.'> width="30" heigth="30" ">';
				}
				} else if ($field_name == ListingCalculator::$iperalSuffix){
				if($value["interesse_merceologico_iperal"] ->getData() == 1){ 
					return  "ok";
					//$res = '<img src="'.ListingCalculator::$urlOkIcon.'> width="30" heigth="30" ">';
				} else {
					//$res = '<img src="'.ListingCalculator::$urlKoIcon.'> width="30" heigth="30" ">';
					return  "ko";
				}
				} else if($field_name == ListingCalculator::$poliSuffix){
				if($value["interesse_merceologico_poli"] ->getData() == 1) {
					//$res = '<img src="'.ListingCalculator::$urlOkIcon.'> width="30" heigth="30" ">';
					return  "ok";
				} else {
					//$res = '<img src="'.ListingCalculator::$urlKoIcon.'> width="30" heigth="30" ">';
					return  "ko";
				}
				} else if($field_name == ListingCalculator::$sogegrossSuffix){
					if($value["interesse_merceologico_sogegross"] ->getData() == 1) {
					//$res = '<img src="'.ListingCalculator::$urlOkIcon.'> width="30" heigth="30" ">';
					return  "ok"; 
				} else {
					//$res = '<img src="'.ListingCalculator::$urlKoIcon.'> width="30" heigth="30" ">';
					return "ko";
				}
				}
				}
				return $res;
	}
}